var koa = require('koa');
var parse = require('co-body')
var zlib = require('zlib')
var app = koa();
var port = process.env.PORT || 3000
// logger
/*
app.use(function *(next){
  var start = new Date;
  yield next;
  var ms = new Date - start;
  console.log('%s %s - %s', this.method, this.url, ms);
});
*/

// response

app.use(function *(){
  data =  yield parse(this, { limit: '10000kb' })
  console.log(data);
  // var buffer = Buffer.from(data.toString(), 'base64');
/*
  var buffer = data.toString()
  this.body = yield new Promise((resolve, reject)=>{
      zlib.gunzip(buffer, (err, buffer) => {
          if (!err) {
            resolve(buffer.toString())
          } else {
            reject(err)
          }
        });
  })
  console.log(this.body);
  */
  this.body = data
});

app.listen(port);
